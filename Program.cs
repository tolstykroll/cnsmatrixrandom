﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cnsMatrixRandom
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Random random = new Random();

            int randI = random.Next(1, 10);
            int randJ = random.Next(1, 10);

            for (int i = 0; i < randI; i++)
            {
                for (int j = 0; j < randJ; j++)
                {
                    Console.Write($"{random.Next(0, 9)} ");
                }
                Console.Write($"{random.Next(0, 9)} ");
                Console.WriteLine();
            }
        }
    }
}
